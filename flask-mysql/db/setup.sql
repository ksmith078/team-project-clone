DROP DATABASE digz_db;
CREATE DATABASE digz_db;
USE digz_db;
DROP TABLE IF EXISTS Starred, User, Pic, Room, Agent;

CREATE TABLE User (
	id INT AUTO_INCREMENT,
	email VARCHAR(45) UNIQUE NOT NULL,
	username VARCHAR(15) UNIQUE NOT NULL,
    is_admin BOOLEAN DEFAULT 0,
	password VARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE Agent (
	id INT AUTO_INCREMENT,
    agent_name VARCHAR(45) UNIQUE,
	email VARCHAR(45) UNIQUE NOT NULL,
	phone CHAR(11),
	PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE Room (
	id INT AUTO_INCREMENT,
	agent_id INT,
    type ENUM("houseshare", "flatshare", "campus halls", "off-campus halls"),
    bed ENUM("single", "double", "twin"),
    price INT,
    billing_period ENUM("pw", "pcm"),
    locality VARCHAR(45),
	when_posted TIMESTAMP,
    description TEXT,
	PRIMARY KEY (id),
    FOREIGN KEY (agent_id) REFERENCES Agent (id)
) ENGINE=InnoDB;

CREATE TABLE Pic (
	id INT AUTO_INCREMENT,
	url VARCHAR(200) NOT NULL,
	room_id INT,
	PRIMARY KEY (id),
    FOREIGN KEY (room_id) REFERENCES Room (id)
) ENGINE=InnoDB;

CREATE TABLE Starred (
    user_id INT NOT NULL,
    room_id INT NOT NULL,
	PRIMARY KEY (user_id, room_id),
    FOREIGN KEY (user_id) REFERENCES User (id),
    FOREIGN KEY (room_id) REFERENCES Room (id)
) ENGINE=InnoDB;

INSERT INTO User (email, username, password, is_admin) VALUES
  ("bob@bob.com", "scarface", "def123B-2", 0),
  ("goofy@hotmail.com", "goofy", "g00f8a11", 0),
  ("lala@tinkywinky.com", "lala", "ttsay88", 0),
  ("shushu@gmail.com", "shushu", "jiggy_66", 0),
  ("admin@digz.com", "admin", "pass123", 1);

INSERT INTO Agent (agent_name, phone, email) VALUES
	("Leeds Trinity University", "01134673900", "accom@leedstrinity.ac.uk"),
	("Droopy's", "01132134410", "rentals@droopys.co.uk"),
    ("Headingley Digs", "01132171234", "info@headingleydigs.co.uk"),
    ("Private Landlord", "01132300141", "123@ltudigs.ac.uk");
    
INSERT INTO Room (agent_id, type, bed, price, billing_period, locality, description, when_posted) VALUES
	(1, "campus halls", "single", 139, "pw", "Horsforth", "Part-catered single rooms available in our campus halls.", "2019-01-10 09:07:22"),
    (1, "off-campus halls", "single", 129, "pw", "Horsforth", "Want to live within minutes of the main campus facilities, but in a peaceful loaction surrounded by greenery? Look no further than Trinity Close - a community of self-catered flats with 6 en-suite bedrooms in each.", NOW()),
    (2, "houseshare", "double", 100, "pw", "Burley Park", "Generous double bedroom in shared house, conveniently located within 5 minutes walk of Burley Park train station. All bills included!", NOW()),
    (4, "flatshare", "double", 500, "pcm", "Hyde Park", "Spacious double in friendly flatshare. Couples considered.",  "2019-02-11 12:01:34"),
    (2, "houseshare", "single", 400, "pcm", "Kirkstall", "Cosy single room close to Kirkstall train station. Join a vibrant and fun-loving household.", NOW()),
    (3, "houseshare", "double", 575, "pcm", "Headingley", "Considerate and studious housemate wanted to fill spacious en-suite double just 5 minutes walk from Headingley centre. No couples please.", NOW()),
    (3, "flatshare", "single", 450, "pcm", "Horsforth", "Room available in small but well maintained flat. Close to Horsforth train station and local amenities and woodland.", NOW());

INSERT INTO Pic (room_id, url) VALUES
	(1, "https://farm5.staticflickr.com/4135/4900473063_d095bba159_b.jpg"),
	(7, "https://photos2.spareroom.co.uk/images/flatshare/listings/large/37/61/37612840.jpg"),
	(2, "https://media.studentcrowd.net/w426-h284-q70-cfill/1431541023_found.jpg.webp"),
    (2, "https://c1.staticflickr.com/5/4080/4900474713_2385ee783d_b.jpg"),
    (3, "https://photos2.spareroom.co.uk/images/flatshare/listings/large/22/87/22875064.jpg"),
	(4, "https://photos2.spareroom.co.uk/images/flatshare/listings/large/41/06/41064961.jpg"),
    (5, "https://photos2.spareroom.co.uk/images/flatshare/listings/large/30/77/30772477.jpg"),
    (6, "https://static.erm-assets.com/r1-97-3-318/dynamicimages/resize/uk-erm/H180416142837589/1/1024.jpg?modified=20180416133726");
    
INSERT INTO Starred (user_id, room_id) VALUES
	(1, 1),
    (1, 3),
	(2, 3);
