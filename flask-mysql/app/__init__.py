from flask import Flask
from config import Config
# import the MySQL extension

# instantiate flask app
app = Flask(__name__)

# apply configuration

# instantiate a MySQL database server object

# configure access to the database server

# import routes
from app import routes
