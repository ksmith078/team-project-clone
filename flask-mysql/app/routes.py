from flask import render_template, jsonify
from app import app
# import mysql instance from app

# helper function
def get_rows(cursor):
    """ gets query results from cursor as
        list of dicts
    """
    fields = [i[0] for i in cursor.description]
    return [ dict(zip(fields, row)) for row in list(cursor.fetchall())]

@app.route('/')
def home():
    # obtain a database cursor
    # execute queries to fetch rooms and pics data...
    rooms = []
    pics = []
    return render_template('home.html', page='Home', rooms=rooms, pics=pics)

@app.route('/dash')
def dash():
    # obtain a database cursor
    cursor = mysql.get_db().cursor()
    # execute queries...
    cursor.execute("SELECT CEILING(AVG(price)) AS avg_price, locality FROM Room WHERE billing_period='pw' GROUP BY locality")
    data = get_rows(cursor)
    return render_template('dashboard.html', page='Dashboard', avg_prices=data)

if __name__ == '__main__':
  app.run(debug=True)
