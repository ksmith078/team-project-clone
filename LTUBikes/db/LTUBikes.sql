DROP DATABASE LTUBikes;
CREATE DATABASE LTUBikes;
USE LTUBikes;
DROP TABLE IF EXISTS User, ContactDetails, Stats;

CREATE TABLE User (
	ID INT PRIMARY KEY AUTO_INCREMENT,
    FullName VARCHAR(25) NOT NULL,
	Username VARCHAR(15) UNIQUE NOT NULL,
    Email VARCHAR(45) UNIQUE NOT NULL,
    Password VARCHAR(25) NOT NULL,
    IsAdmin BOOLEAN DEFAULT 0
) ENGINE=InnoDB;

CREATE TABLE ContactDetails (
	ID INT PRIMARY KEY AUTO_INCREMENT,
    PostCode VARCHAR(10) UNIQUE NOT NULL,
	PhoneNumber VARCHAR(20) UNIQUE NOT NULL,
    Address VARCHAR(100) UNIQUE NOT NULL,
    UserID INT NOT NULL REFERENCES User(ID)
) ENGINE=InnoDB;

CREATE TABLE Stats (
	ID INT PRIMARY KEY AUTO_INCREMENT,
    TotalDist DECIMAL(4,2) UNSIGNED,
	WeeklyDist DECIMAL(4,2) UNSIGNED,
    AvgSpeed DECIMAL(4,2) UNSIGNED,
    AvgCaloriesBurned DECIMAL(4,2) UNSIGNED,
    TotalRentals INT,
    TotalHours DECIMAL(4,2) UNSIGNED,
    TotalSpend DECIMAL(4,2) UNSIGNED,
    UserID INT NOT NULL REFERENCES User(ID)
) ENGINE=InnoDB;

INSERT INTO User (FullName, Username, Email, Password, IsAdmin) VALUES
    ("Bob Wilson", "scarface", "bob@bob.com", "def123B-2", 0),
    ("Geoff Smith", "goofy", "goofy@hotmail.com", "g00f8a11", 0),
    ("Emily Stevens", "lala", "lala@tinkywinky.com", "ttsay88", 0),
    ("Chris Davies", "shushu", "shushu@gmail.com", "jiggy_66", 0),
    ("Max Burleigh", "admin",  "admin@digz.com", "pass123", 1);

INSERT INTO ContactDetails (PostCode, PhoneNumber, Address, UserID) VALUES
    ("LS29 9AN", "07893 896011", "20 Kings Road, Ilkley, West Yorkshire", 2),
    ("LS29 9NE", "07893 895033", "19 Kings Road, Ilkley, West Yorkshire", 3);

INSERT INTO Stats (TotalDist, WeeklyDist, AvgSpeed, AvgCaloriesBurned, TotalRentals, TotalHours, TotalSpend, UserID) VALUES
    (33.43, 12.32, 10.50, 50.60, 8, 13.32, 13.50, 1);